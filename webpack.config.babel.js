module.exports = {
    entry: {
        "tssjsnativebundle": ["./src/index.js"]
    },

    module: {
        loaders: [{
            exclude: /(node_modules|bower_components)/,
            loader: 'babel'
        }]
    },

    output: {
        path: __dirname + '/dist/',
        filename: 'index.js',
        publicPath: '/'
    },

    devServer: {
        contentBase: "./demo",
        open: true,
        port: 3333,
        hot: true,
        inline: true
    }
};
