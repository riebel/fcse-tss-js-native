import {lory} from 'lory.js';

/** Class representing a carousel. */
class Carousel {
    /**
     * Create a carousel.
     * @param {object} options - The options object.
     */
    constructor(options = {}) {
        this.slideable = true;
        this.slider = false;
        // default settings
        const defaultSettings = {
            delay: 10000,
            querySelector: '.js_slider',
            // infinite: 1,
            rewind: true,
            enableMouseEvents: true,
            indicatorsSelector: '.carousel-indicators'
        };
        this.settings = Object.assign({}, defaultSettings, options);

        this.initialize();

    }

    initialize() {
        const sliderElement = document.querySelector(this.settings.querySelector);
        const sliderIndicators = sliderElement.querySelectorAll('.carousel-indicators li');

        if (this.slider) {
            this.slider.destroy();
        }

        // creating slider
        this.slider = lory(sliderElement, this.settings);

        // adding click event listeners to indicators
        for (let i = 0; i < sliderIndicators.length; i++) {
            sliderIndicators[i].addEventListener('click', (e) => {
                // slide to clicked index
                this.slider.slideTo(parseInt(e.target.dataset.slideTo));
            }, false);
        }

        // adding before.lory.slide event listener to window to update indicators
        sliderElement.addEventListener('after.lory.slide', () => {
            // remove all active classes on indicators
            for (let i = 0; i < sliderIndicators.length; i++) {
                sliderIndicators[i].className = '';
            }
            // set active class to next slide indicator
            sliderIndicators[this.slider.returnIndex()].className = 'active';
        });

        // listening if sliding should be stopped
        sliderElement.addEventListener('slider:stop', () => {
            this.slideable = false;
        });

        // listening if sliding should be resume
        sliderElement.addEventListener('slider:resume', () => {
            this.slideable = true;
        });

        // listening if slider should setup the slides based on DOM if DOM or user options have changed or
        // eventlisteners needs to be rebinded
        sliderElement.addEventListener('slider:setup', () => {
            // this.slider.setup();
            this.initialize();
        });

        // interval for auto slide
        setInterval(() => {
            // check if slider is being hovered
            if (sliderElement.parentElement.querySelector(':hover') !== sliderElement && this.slideable) {
                this.slider.next();
            }
        }, this.settings.delay);
    }
}

new Carousel();